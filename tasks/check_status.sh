#!/usr/bin/env bash

service=$PT_service
if [[ -z $service ]]; then
  out=$(curl https://localhost:8140/status/v1/services -k -s)
else
  out=$(curl -k -s https://localhost:8140/status/v1/services/${service})
fi

code=$?
echo $out
exit $code