#!/usr/bin/env bash

# Reset the console password using the puppet infra command

/opt/puppetlabs/bin/puppet infra console_password --password $PT_console_password
