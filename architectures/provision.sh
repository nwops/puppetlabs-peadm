#!/usr/bin/env bash
# bundle install
# bundle exec rake spec_prep
# must be in the architectures/standard directory
echo 'Please chose a PE architecture to build: '
downloads=$(realpath ./)
mod_path=$(realpath ../)
select opt in */
do
  cd $opt
  docker-compose up -d --build
  docker-compose run -v ${downloads}:/downloads -v ${mod_path}/spec/fixtures/modules:/modules -v ${mod_path}:/mods/peadm bolt plan run peadm::provision \
  --inventory inventory.yaml \
  --modulepath=/modules:/mods \
  --params @params.json 
  break;
   
done

