#!/usr/bin/env bash
# bundle install
# bundle exec rake spec_prep
# must be in the architectures/standard directory
docker-compose up --build -d
downloads=$(realpath ../)
mod_path=$(realpath ../../) # required due to symlink in fixtures
docker-compose run -v ${downloads}:/downloads -v ${mod_path}/spec/fixtures/modules:/modules -v ${mod_path}:/mods/peadm bolt plan run peadm::provision \
   --inventory inventory.yaml \
   --modulepath=/modules:/mods \
   --params @params.json 

# There is an issue with the password not being set correctly
# docker-compose exec standard_aio puppet infra console_password --password puppetlabs   
